export const SELECT_CONTACT = "APP/UI/SELECT_CONTACT";

export const initialState = {
  selectedContact: null,
};

export const selectContact = (contact) => ({
  type: SELECT_CONTACT,
  contact,
});

export const uiReducer = (state = initialState, action) => {
  console.log("w reducer", action);
  if (action.type === SELECT_CONTACT) {
    return {
      ...state,
      selectedContact: action.contact,
    };
  }
};
