import React, { useMemo, useRef, useEffect } from "react";
import useWebSocket, { ReadyState } from "react-use-websocket";

import Head from "next/head";
import styles from "../styles/chat.module.scss";

import { StoreProvider } from "../store/Store";
import { initialState, uiReducer } from "../store/ui.reducer";

import { MessageWindow } from "../components/MessageWindow";
import { UserList } from "../components/UserList";

const socketUrl = "ws://localhost:3300";

export default function ChatWindow() {
  const { sendJsonMessage, lastJsonMessage, readyState } = useWebSocket(socketUrl, {
    shouldReconnect: (closeEvent) => true,
  });

  const userList = useRef([]);
  // const messageHistory = useRef([]);

  // messageHistory.current = useMemo(() => messageHistory.current.concat(lastJsonMessage), [lastJsonMessage]);

  useMemo(() => __processReceivedMessage(lastJsonMessage), [lastJsonMessage]);
  useEffect(() => onLoadContacts(), []);

  const connectionStatus = {
    [ReadyState.CONNECTING]: "Connecting",
    [ReadyState.OPEN]: "Open",
    [ReadyState.CLOSING]: "Closing",
    [ReadyState.CLOSED]: "Closed",
    [ReadyState.UNINSTANTIATED]: "Uninstantiated",
  }[readyState];

  function onLoadContacts() {
    console.log("wwwonononload");
    sendJsonMessage({ type: "loadUserList", payload: "" });
  }

  function onSendMsg(contact, msg) {
    console.log("wysylam", msg);
    sendJsonMessage({ type: "msg", payload: msg });
  }

  return (
    <React.StrictMode>
      <StoreProvider initialState={initialState} reducer={uiReducer}>
        <div className={styles.container}>
          <Head>
            <title>Czat wp</title>
          </Head>

          <main>
            <h1 className="title">Chat zadanie</h1>

            <UserList userList={userList.current} />
            <MessageWindow onSendMsg={onSendMsg} lastMsg={lastJsonMessage} />
          </main>
          {/* <h4>The WebSocket is currently {connectionStatus}</h4> */}
        </div>
      </StoreProvider>
    </React.StrictMode>
  );

  function __processReceivedMessage(lastJsonMessage) {
    console.log("processing message");
    if (lastJsonMessage?.type) {
      switch (lastJsonMessage.type) {
        case "userList":
          userList.current = JSON.parse(lastJsonMessage.payload);
          break;
        case "msg":
          console.log("received msg", lastJsonMessage.payload);
          break;
      }
    }
  }
}

{
}
