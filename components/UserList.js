import React from "react";
import styles from "../styles/chat.module.scss";

import { useStore } from "../store/Store";
import { selectContact } from "../store/ui.reducer";

export const UserList = ({ userList }) => {
  if (!Array.isArray(userList)) return null;

  const [store, dispatch] = useStore();

  function onContactClick(u) {
    dispatch(selectContact(u));
  }

  return (
    <div className={styles.userList}>
      <ul>
        {userList.map((u, i) => (
          <li key={i} onClick={() => onContactClick(u)}>
            <img src={u.photo} />
            <div className={styles.name}>
              {u.firstName} {u.lastName}
            </div>
            <div className={styles.status}>{u.status}</div>
          </li>
        ))}
      </ul>

      {/* <h3>Wybrany w userlist {store.selectedContact?.firstName}</h3> */}
    </div>
  );
};
