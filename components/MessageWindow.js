import React, { useState } from "react";

import { useStore } from "../store/Store";
import styles from "../styles/chat.module.scss";

export const MessageWindow = ({ onSendMsg, lastMsg }) => {
  const [store] = useStore();
  const [msg, setMsg] = useState("");

  function onInputMsgChange(e) {}
  return (
    <div className={styles.messageWindow}>
      <h3>Rozmowa z: {store.selectedContact?.firstName}</h3>
      {lastMsg?.type === "msg" && <div className={styles.response}>{lastMsg?.payload}</div>}
      {lastMsg?.type !== "msg" && <div className={styles.response}></div>}
      <div className={styles.input}>
        <textarea placeholder="Your msg..." type="text" onChange={(e) => setMsg(e.target.value)}></textarea>
        <button onClick={() => onSendMsg(store.selectedContact, msg)}>Send</button>
      </div>
    </div>
  );
};
