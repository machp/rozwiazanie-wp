const axios = require("axios");

const JOKEURL = "https://v2.jokeapi.dev/joke/Any?type=single";

class RandomJoke {
  async getJoke() {
    try {
      const res = await axios.get(JOKEURL);
      return res.data.joke;
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = RandomJoke;
