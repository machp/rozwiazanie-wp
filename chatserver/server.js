const WebSocket = require("ws");
const ChatUserList = require("./ChatUserList.js");
const RandomJoke = require("./RandomJoke.js");

const wss = new WebSocket.Server({ port: 3300 });
const userList = new ChatUserList();

wss.on("connection", function connection(ws, req) {
  ws.on("message", async function incoming(message) {
    try {
      const jsonMessage = JSON.parse(message);
      let { type, payload } = jsonMessage;

      console.log(`received: ${type} ${payload}`);

      switch (type) {
        case "loadUserList":
          response = { type: "userList", payload: JSON.stringify(userList.list) };
          break;
        case "msg":
          const randomJoke = new RandomJoke();
          const joketext = await randomJoke.getJoke();
          console.log(joketext);
          response = { type: "msg", payload: joketext };
          break;
        default:
          return;
      }
    } catch (e) {
      console.error("Catch:", e);
    }
    ws.send(JSON.stringify(response));
  });

  console.log("Client connected", req.socket.remoteAddress);
});

// type Msg {
//     type: string,
//     payload: string
// }
