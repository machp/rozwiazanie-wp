const fs = require("fs");

class ChatUserList {
  constructor() {
    this.list = [];
    this.loadUserList();
  }

  loadUserList() {
    /* uifaces requires credit card, using sample data mock instead: */
    let uifacesRaw = fs.readFileSync("./chatserver/mocks/uifaces.json");
    let uifaces = JSON.parse(uifacesRaw);
    this.list = this.uifaces2ChatUserList(uifaces);
    console.log(`Loaded userlist with ${this.list.length} users`);
  }

  uifaces2ChatUserList(uifaces) {
    return uifaces.map((u) => {
      let [firstName, lastName] = u.name.split(" ");
      return {
        firstName,
        lastName,
        photo: u.photo,
        status: 0,
      };
    });
  }
}

module.exports = ChatUserList;

//  type User {
//   firstName: string,
//   lastName: string,
//   photo: string
//   status: number
//  }
