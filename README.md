### 0. npm install

### 1. start chatserver

    npm run devcharserver

### 2. on second terminal start chat client - Next.js app

    npm run dev

![screenshot](screenshot.png)


### mój komentarz:


Normalny chat powinen być zrobiony bardziej uniwersalnie, ale i czasochłonnie, ale zgodnie z treścią zadania, zastosowałem uproszczenia:

- aplikacja nie rozpoznaje uzytkownika jako nowego na liście, nie ma opcji ustawiania nicka itp

- jeśli kilka osób się połączy, nie zmienia to listy, jest stała.

- odpowiadanie dowcipami nie jest zaimplemnowane jako Bot emulujący uzytkownika, bo nie ma takich załozen

- obrazki się nie skalują na serwerze


A z braku czasu (urlop):

- brak dopracowania UI, historii wiadomości, ssr(next.js wspiera ale nie mam teraz chwili) itp